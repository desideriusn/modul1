/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pertemuan1;

/**
 *
 * @author Administrator
 */
public class Main {

    public static void main(String[] args) {
        UKM ukm = new UKM();
        ukm.setNamaUnit("Taekwondo");

        Mahasiswa ketua = new Mahasiswa();
        ketua.setNama("Nadia");
        ketua.setNim("171133747");
        ketua.setTanggalLahir("19 Agustus 1999");
        ukm.setKetua(ketua);
        System.out.println("UKM         : " + ukm.getNamaUnit());
        System.out.println("");
        System.out.println("Ketua UKM   : " + ukm.getKetua().nama);
        System.out.println("NIM         : " + ukm.getKetua().getNim());
        System.out.println("TTL         : " + ukm.getKetua().tanggalLahir);
        System.out.println("");

        Mahasiswa sekretaris = new Mahasiswa();
        sekretaris.setNama("Ristya");
        sekretaris.setNim("185413432");
        sekretaris.setTanggalLahir("24 Desember 2000");
        ukm.setSekretaris(sekretaris);
        System.out.println("Sekretaris  : " + ukm.getSekretaris().nama);
        System.out.println("NIM         : " + ukm.getSekretaris().getNim());
        System.out.println("TTL         : " + ukm.getSekretaris().tanggalLahir);
        System.out.println("====================================================================");

        Penduduk[] penduduk = new Penduduk[2];
        penduduk[0] = new Mahasiswa();
        Mahasiswa anggotaMhs = (Mahasiswa) penduduk[0];
        anggotaMhs.setNama("Niyo");
        anggotaMhs.setNim("185314117");
        anggotaMhs.setTanggalLahir("23 Mei 2000");

        penduduk[1] = new MasyarakatSekitar();
        MasyarakatSekitar anggotaMas = (MasyarakatSekitar) penduduk[1];
        anggotaMas.setNama("Rara");
        anggotaMas.setNomor("104");
        anggotaMas.setTanggalLahir("20 September 2000");

        double total = 0;
        ukm.setAnggota(penduduk);
        System.out.printf("%-4s", "No.");
        System.out.printf("%-10s", "Nama");
        System.out.printf("%-10s", "NIM");
        System.out.printf("%-10s", "Nomor");
        System.out.printf("%-20s", "Tanggal Lahir");
        System.out.printf("%-10s", "Iuran Anggota");
        System.out.println("");
        System.out.println("--------------------------------------------------------------------");

        for (int i = 0; i < penduduk.length; i++) {
            if (ukm.getAnggota()[i] instanceof Mahasiswa) {
                Mahasiswa mahasiswa = (Mahasiswa) ukm.getAnggota()[i];
                System.out.printf("%-4s", (i + 1) + ".");
                System.out.printf("%-10s", ukm.getAnggota()[i].getNama());
                System.out.printf("%-10s", mahasiswa.getNim());
                System.out.printf("%-10s", "-");
                System.out.printf("%-20s", ukm.getAnggota()[i].getTanggalLahir());
                System.out.printf("%-10s", "Rp " + ukm.getAnggota()[i].hitungIuran());
                total = total + ukm.getAnggota()[i].hitungIuran();
                System.out.println("");

            } else if (ukm.getAnggota()[i] instanceof MasyarakatSekitar) {
                MasyarakatSekitar masSek = (MasyarakatSekitar) ukm.getAnggota()[i];
                System.out.printf("%-4s", (i + 1) + ".");
                System.out.printf("%-10s", ukm.getAnggota()[i].getNama());
                System.out.printf("%-10s", "-");
                System.out.printf("%-10s", masSek.getNomor());
                System.out.printf("%-20s", ukm.getAnggota()[i].getTanggalLahir());
                System.out.printf("%-10s", "Rp " + ukm.getAnggota()[i].hitungIuran());
                total = total + ukm.getAnggota()[i].hitungIuran();
                System.out.println("");
            }
        }
        System.out.println("====================================================================");
        System.out.println("Total Iuran = Rp " + total);
    }
}
