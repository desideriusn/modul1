/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pertemuan1;

/**
 *
 * @author Administrator
 */
public class MasyarakatSekitar extends Penduduk {

    private String nomor;

    public MasyarakatSekitar() {

    }

    public MasyarakatSekitar(String nomor) {
        this.nomor = nomor;
    }

    public void setNomor(String nomor) {
        this.nomor = nomor;
    }

    public String getNomor() {
        return nomor;
    }

    @Override
    public double hitungIuran() {
        int nomor2 = Integer.parseInt(nomor);
        double iuranMasyarakatUmum = nomor2 * 100;
        return iuranMasyarakatUmum;
    }
}
