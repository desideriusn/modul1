/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pertemuan1;

/**
 *
 * @author Administrator
 */
public class Mahasiswa extends Penduduk {

    private String nim;

    public Mahasiswa() {

    }

    public Mahasiswa(String nim) {
        this.nim = nim;
    }

    public void setNim(String nim) {
        this.nim = nim;
    }

    public String getNim() {
        return nim;
    }

    @Override
    public double hitungIuran() {
        int nim2 = Integer.parseInt(nim); //mengubah tipe data string ke int
        double iuranMhs = nim2 / 10000;
        return iuranMhs;
    }
}
